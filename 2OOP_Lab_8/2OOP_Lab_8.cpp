﻿// Напишите программу, которая циклически будет запрашивать ввод пользователем двух денежных сумм, выраженных в фунтах, шиллингах и пен -сах(см.упражнения 10 и 12 главы 2).Программа должна складывать введенные суммы и выводить на экран результат, также выраженный в фунтах, шиллингах и пенсах.После каждой итерации программа должна спрашивать пользователя, желает ли он продолжать работу программы. При этом рекомендуется использовать цикл do.
#include "pch.h"
#include <iostream>
#include <iomanip> 
using namespace std;
int main()
{
	setlocale(0, "");
	int funt, funt1, shill, shill1, pens, pens1, funt2, shill2, pens2;
	char ch;
	do {
		cout << " Введите первую сумму: " << endl;
		cin >> funt1 >> ch >> shill1 >> ch >> pens1;
		cout << " Введите вторую сумму: " << endl;
		cin >> funt2 >> ch >> shill2 >> ch >> pens2;
		funt = funt1 + funt2;
		shill = shill1 + shill2;
		if (shill > 19)
		{
			funt++;
			shill = shill - 20;
		}
		pens = pens1 + pens2;
		if (pens > 11)
		{
			shill++;
			pens = pens - 12;
		}
		cout << " Всего: " << funt << ch << shill << ch << pens << endl;
		cout << " Продолжить ? (y/n)" << endl;
		cin >> ch;
	} while (ch != 'n');
	system("pause");
	return 0;
}
